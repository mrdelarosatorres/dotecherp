import { Connection } from 'typeorm';
import { Moneda } from '../../models/Moneda';
import { GenericInterface } from '../../interfaces/GenericInterface';
export declare class MonedaService implements GenericInterface<Moneda> {
    private conn;
    constructor(conn: Connection);
    findAll(): Promise<Moneda[]>;
}
