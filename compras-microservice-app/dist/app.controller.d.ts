import { AppService } from './app.service';
export declare class AppController {
    private readonly appService;
    constructor(appService: AppService);
    handleMessagePrinted(data: Record<string, unknown>): Promise<void>;
    filterProducts(data: Record<string, unknown>): Promise<void>;
    addProducts(data: Record<string, unknown>): Promise<void>;
}
