"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let Moneda = class Moneda {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: 'id_moneda' }),
    __metadata("design:type", Number)
], Moneda.prototype, "idMoneda", void 0);
__decorate([
    typeorm_1.Column({ name: 'desc_moneda', length: 5 }),
    __metadata("design:type", String)
], Moneda.prototype, "descMoneda", void 0);
__decorate([
    typeorm_1.Column({ name: 'clave_moneda' }),
    __metadata("design:type", String)
], Moneda.prototype, "claveMoneda", void 0);
__decorate([
    typeorm_1.Column({ name: 'activo' }),
    __metadata("design:type", Boolean)
], Moneda.prototype, "activo", void 0);
Moneda = __decorate([
    typeorm_1.Entity({ name: 'cat_moneda' })
], Moneda);
exports.Moneda = Moneda;
//# sourceMappingURL=Moneda.js.map