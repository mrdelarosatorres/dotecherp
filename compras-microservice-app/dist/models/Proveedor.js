"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const typeorm_1 = require("typeorm");
let Proveedor = class Proveedor {
};
__decorate([
    typeorm_1.PrimaryGeneratedColumn({ name: 'id_proveedor' }),
    __metadata("design:type", Number)
], Proveedor.prototype, "idProveedor", void 0);
__decorate([
    typeorm_1.Column({ name: 'razon_social', length: 200 }),
    __metadata("design:type", String)
], Proveedor.prototype, "razonSocial", void 0);
__decorate([
    typeorm_1.Column({ name: 'activo' }),
    __metadata("design:type", Boolean)
], Proveedor.prototype, "activo", void 0);
Proveedor = __decorate([
    typeorm_1.Entity({ name: 'cat_proveedores' })
], Proveedor);
exports.Proveedor = Proveedor;
//# sourceMappingURL=Proveedor.js.map