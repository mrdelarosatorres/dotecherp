export declare class Moneda {
    idMoneda: number;
    descMoneda: string;
    claveMoneda: string;
    activo: boolean;
}
