export declare class Proveedor {
    idProveedor: number;
    razonSocial: string;
    activo: boolean;
}
