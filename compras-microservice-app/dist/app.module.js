"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const app_controller_1 = require("./app.controller");
const app_service_1 = require("./app.service");
const typeorm_1 = require("@nestjs/typeorm");
const Moneda_1 = require("./models/Moneda");
const Product_1 = require("./models/Product");
const product_module_1 = require("./modules/productos/product.module");
const Proveedor_1 = require("./models/Proveedor");
const database_service_1 = require("./config/app/database/database.service");
const database_module_1 = require("./config/app/database/database.module");
const config_module_1 = require("./config/app/config/config.module");
const config_1 = require("@nestjs/config");
const gasto_module_1 = require("./modules/gastos/gasto.module");
let AppModule = class AppModule {
};
AppModule = __decorate([
    common_1.Module({
        imports: [
            config_1.ConfigModule.forRoot({
                envFilePath: '.development.env',
                isGlobal: true,
            }),
            typeorm_1.TypeOrmModule.forRootAsync({
                imports: [config_1.ConfigModule],
                inject: [config_1.ConfigService],
                useFactory: (configService) => ({
                    type: 'postgres',
                    host: configService.get('DB_HOST'),
                    port: configService.get('DB_PORT'),
                    username: configService.get('DB_USER'),
                    password: configService.get('DB_PWD'),
                    database: configService.get('DB_NAME'),
                    entities: [
                        Product_1.Product, Moneda_1.Moneda, Proveedor_1.Proveedor
                    ],
                    synchronize: false,
                })
            }),
            product_module_1.ProductModule,
            config_module_1.AppConfigModule,
            database_module_1.DatabaseModule,
            config_module_1.AppConfigModule,
            gasto_module_1.GastoModule,
        ],
        controllers: [app_controller_1.AppController],
        providers: [app_service_1.AppService, database_service_1.DatabaseService],
    })
], AppModule);
exports.AppModule = AppModule;
//# sourceMappingURL=app.module.js.map