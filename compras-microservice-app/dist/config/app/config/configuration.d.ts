declare const _default: (() => {
    env: string;
    name: string;
    host: string;
    port: string;
}) & import("@nestjs/config").ConfigFactoryKeyHost;
export default _default;
