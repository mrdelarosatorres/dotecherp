declare const _default: (() => {
    env: string;
    host: string;
    port: string;
    name: string;
    user: string;
    pwd: string;
}) & import("@nestjs/config").ConfigFactoryKeyHost;
export default _default;
