import { ConfigService } from '@nestjs/config';
export declare class DatabaseService {
    private configService;
    constructor(configService: ConfigService);
    get name(): string;
    get env(): string;
    get host(): string;
    get port(): number;
    get user(): number;
    get pdw(): number;
}
