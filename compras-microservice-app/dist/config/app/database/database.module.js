"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Joi = require("@hapi/joi");
const common_1 = require("@nestjs/common");
const database_service_1 = require("./database.service");
const config_1 = require("@nestjs/config");
const configuration_1 = require("./configuration");
let DatabaseModule = class DatabaseModule {
};
DatabaseModule = __decorate([
    common_1.Module({
        imports: [
            config_1.ConfigModule.forRoot({
                envFilePath: '.development.env',
                isGlobal: true,
            }),
            config_1.ConfigModule.forRoot({
                load: [configuration_1.default],
                validationSchema: Joi.object({
                    DB_NAME: Joi.string().default('db_name'),
                    DB_ENV: Joi.string()
                        .valid('development', 'production', 'test', 'provision')
                        .default('development'),
                    DB_HOST: Joi.string().default('127.0.0.1'),
                    DB_PORT: Joi.number().default(5432),
                    DB_USER: Joi.string().default('user'),
                    DB_PWD: Joi.string().default('pwd'),
                }),
            }),
        ],
        providers: [config_1.ConfigService, database_service_1.DatabaseService],
        exports: [config_1.ConfigService, database_service_1.DatabaseService],
    })
], DatabaseModule);
exports.DatabaseModule = DatabaseModule;
//# sourceMappingURL=database.module.js.map