import { GenericInterface } from '../../interfaces/GenericInterface';
import { Proveedor } from '../../models/Proveedor';
import { Connection } from 'typeorm';
export declare class ProveedorService implements GenericInterface<Proveedor> {
    private conn;
    constructor(conn: Connection);
    nuevo(): Promise<Proveedor>;
    findAll(): Promise<Proveedor[]>;
}
