import { ProveedorService } from './proveedor.service';
import { Proveedor } from '../../models/Proveedor';
export declare class ProveedorController {
    private proveedorService;
    constructor(proveedorService: ProveedorService);
    findAll(data: any): Promise<Proveedor[]>;
    nuevoProveedor(data: any): Promise<Proveedor>;
}
