"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const proveedor_service_1 = require("./proveedor.service");
const microservices_1 = require("@nestjs/microservices");
let ProveedorController = class ProveedorController {
    constructor(proveedorService) {
        this.proveedorService = proveedorService;
    }
    async findAll(data) {
        return this.proveedorService.findAll();
    }
    async nuevoProveedor(data) {
        return this.proveedorService.nuevo();
    }
};
__decorate([
    microservices_1.MessagePattern({ cmd: 'listProveedores' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProveedorController.prototype, "findAll", null);
__decorate([
    microservices_1.MessagePattern({ cmd: 'nuevoProveedor' }),
    __metadata("design:type", Function),
    __metadata("design:paramtypes", [Object]),
    __metadata("design:returntype", Promise)
], ProveedorController.prototype, "nuevoProveedor", null);
ProveedorController = __decorate([
    common_1.Controller('proveedor'),
    __metadata("design:paramtypes", [proveedor_service_1.ProveedorService])
], ProveedorController);
exports.ProveedorController = ProveedorController;
//# sourceMappingURL=proveedor.controller.js.map