"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
Object.defineProperty(exports, "__esModule", { value: true });
const common_1 = require("@nestjs/common");
const Proveedor_1 = require("../../models/Proveedor");
const typeorm_1 = require("typeorm");
let ProveedorService = class ProveedorService {
    constructor(conn) {
        this.conn = conn;
    }
    nuevo() {
        let provedorRepository = this.conn.getRepository(Proveedor_1.Proveedor);
        let proveedor = new Proveedor_1.Proveedor();
        proveedor.razonSocial = 'Proveedor pruebas 2';
        proveedor.activo = true;
        return provedorRepository.save(proveedor);
    }
    findAll() {
        let provedorRepository = this.conn.getRepository(Proveedor_1.Proveedor);
        return provedorRepository.find();
    }
};
ProveedorService = __decorate([
    common_1.Injectable(),
    __metadata("design:paramtypes", [typeorm_1.Connection])
], ProveedorService);
exports.ProveedorService = ProveedorService;
//# sourceMappingURL=proveedor.service.js.map