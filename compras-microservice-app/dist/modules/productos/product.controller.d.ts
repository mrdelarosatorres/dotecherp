import { ProductService } from './product.service';
import { Product } from '../../models/Product';
export declare class ProductController {
    private productService;
    constructor(productService: ProductService);
    listProducts(data: any): Promise<Product[]>;
}
