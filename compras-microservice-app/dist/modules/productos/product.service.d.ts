import { Connection } from 'typeorm';
import { GenericInterface } from '../../interfaces/GenericInterface';
import { Product } from '../../models/Product';
export declare class ProductService implements GenericInterface<Product> {
    private conn;
    constructor(conn: Connection);
    nuevo(): Promise<Product>;
    findAll(): Promise<Product[]>;
}
