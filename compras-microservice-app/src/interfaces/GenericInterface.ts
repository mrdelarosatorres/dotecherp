export interface GenericInterface <T> {
    findAll(): Promise<T[]>;
    nuevo(): Promise<T>
}