import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import {TypeOrmModule} from '@nestjs/typeorm';
import { Moneda } from './models/Moneda';
import { Product } from './models/Product';
import { ProductModule } from './modules/productos/product.module';
import { Proveedor } from './models/Proveedor';
import { DatabaseService } from './config/app/database/database.service';
import { DatabaseModule } from './config/app/database/database.module';
import { AppConfigModule } from './config/app/config/config.module';
import { ConfigService, ConfigModule } from '@nestjs/config';
import { GastoModule } from './modules/gastos/gasto.module';
@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.development.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync(
      {
        imports:[ConfigModule],
        inject: [ConfigService],
        useFactory:(configService: ConfigService) =>({
          type:'postgres',
          host: configService.get('DB_HOST'),
          port: configService.get('DB_PORT'),
          username: configService.get('DB_USER'),
          password: configService.get('DB_PWD'),
          database: configService.get('DB_NAME'),
          entities: [
            Product,Moneda,Proveedor
          ],
          synchronize: false,
        })
      }
    ),
    ProductModule,
    AppConfigModule,
    DatabaseModule,
    AppConfigModule,
    GastoModule,
  ],
  controllers: [AppController],
  providers: [AppService, DatabaseService],
})
export class AppModule {}
