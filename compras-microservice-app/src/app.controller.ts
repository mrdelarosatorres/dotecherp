import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { EventPattern } from '@nestjs/microservices';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @EventPattern('message_printed')
  async handleMessagePrinted(data: Record<string,unknown>) {
    console.log(data.payload);
  }

  @EventPattern('filter_products')
  async filterProducts(data: Record<string,unknown>) {
    console.log(data.text);
  }
  
  @EventPattern('add_products')
  async addProducts(data: Record<string,unknown>) {
    console.log(data.text);
  }
  
}
