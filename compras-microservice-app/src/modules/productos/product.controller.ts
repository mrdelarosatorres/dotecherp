import { Controller } from '@nestjs/common';
import { ProductService } from './product.service';
import { MessagePattern } from '@nestjs/microservices';
import { Product } from '../../models/Product';


@Controller('products')
export class ProductController {
    constructor(private productService: ProductService){

    }
    
  @MessagePattern({cmd:'listProducts'})
  async listProducts(data: any): Promise<Product[]> {
    console.log(data.payload);
    return this.productService.findAll();
  }
}
