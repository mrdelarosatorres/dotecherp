import { Injectable } from '@nestjs/common';
import { Connection } from 'typeorm';
import { GenericInterface } from '../../interfaces/GenericInterface';
import { Product } from '../../models/Product';

@Injectable()
export class ProductService implements GenericInterface<Product> {
    constructor(private conn: Connection) {

    }
    nuevo(): Promise<Product> {
        throw new Error("Method not implemented.");
    }

    findAll(): Promise<Product[]> {
        let productRepository = this.conn.getRepository(Product);
        let product = new Product();
        product.code = 'Product-1';
        productRepository.save(product);
        return productRepository.find();
    }
}
