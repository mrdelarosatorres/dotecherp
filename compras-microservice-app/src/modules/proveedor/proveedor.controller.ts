import { Controller } from '@nestjs/common';
import { ProveedorService } from './proveedor.service';
import { Proveedor } from '../../models/Proveedor';
import { MessagePattern } from '@nestjs/microservices';

@Controller('proveedor')
export class ProveedorController {
    constructor(private proveedorService: ProveedorService){

    }

    @MessagePattern({cmd:'listProveedores'})
    async findAll(data: any): Promise<Proveedor[]> {
        return this.proveedorService.findAll();
    }
    //** Agrega un nuevo proveedor */
    @MessagePattern({cmd:'nuevoProveedor'})
    async nuevoProveedor(data: any): Promise<Proveedor> {
        return this.proveedorService.nuevo();
    }
}
