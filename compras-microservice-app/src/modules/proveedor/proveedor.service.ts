import { Injectable } from '@nestjs/common';
import { GenericInterface } from '../../interfaces/GenericInterface';
import { Proveedor } from '../../models/Proveedor';
import { Connection } from 'typeorm';

@Injectable()
export class ProveedorService implements GenericInterface<Proveedor>{
    constructor(private conn: Connection){

    }

    nuevo(): Promise<Proveedor> {
        let provedorRepository = this.conn.getRepository(Proveedor);
        let proveedor = new Proveedor();
        proveedor.razonSocial = 'Proveedor pruebas 2';
        proveedor.activo = true;
        return provedorRepository.save(proveedor);
    }
    
    findAll(): Promise<Proveedor[]> {
        let provedorRepository = this.conn.getRepository(Proveedor);
        return provedorRepository.find();
    }

}
