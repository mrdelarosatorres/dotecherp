import * as Joi from '@hapi/joi';
import { Module } from '@nestjs/common';
import { DatabaseService } from './database.service';
import { ConfigModule, ConfigService } from '@nestjs/config';
import configuration from './configuration';
@Module({
    imports: [
      ConfigModule.forRoot({
        envFilePath: '.development.env',
        isGlobal: true,
      }),
      ConfigModule.forRoot({
        load: [configuration],
        validationSchema: Joi.object({
          DB_NAME: Joi.string().default('db_name'),
          DB_ENV: Joi.string()
            .valid('development', 'production', 'test', 'provision')
            .default('development'),
          DB_HOST: Joi.string().default('127.0.0.1'),
          DB_PORT: Joi.number().default(5432),
          DB_USER: Joi.string().default('user'),
          DB_PWD: Joi.string().default('pwd'),
        }),
      }),
    ],
    providers: [ConfigService, DatabaseService],
    exports: [ConfigService, DatabaseService],
  })
export class DatabaseModule {}
