import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class DatabaseService {
    constructor(private configService: ConfigService) { }
    get name(): string {
        return this.configService.get<string>('db.name');
    }
    get env(): string {
        return this.configService.get<string>('db.env');
    }
    get host(): string {
        return this.configService.get<string>('db.host');
    }
    get port(): number {
        return Number(this.configService.get<number>('db.port'));
    }
    get user(): number {
        return Number(this.configService.get<number>('db.user'));
    }
    get pdw(): number {
        return Number(this.configService.get<number>('db.pwd'));
    }
}
