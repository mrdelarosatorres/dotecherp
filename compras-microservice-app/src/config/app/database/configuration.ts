import { registerAs } from '@nestjs/config';
export default registerAs('app', () => ({
    env: process.env.DB_ENV,
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    name: process.env.DB_NAME,
    user: process.env.DB_USER,
    pwd: process.env.DB_PWD,
}));