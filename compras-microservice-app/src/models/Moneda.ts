import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:'cat_moneda'})
export class Moneda {
    @PrimaryGeneratedColumn({name:'id_moneda'})
    idMoneda: number;
    @Column({name:'desc_moneda',length:5})
    descMoneda: string;
    @Column({name:'clave_moneda'})
    claveMoneda: string;
    @Column({name:'activo'})
    activo: boolean;
}