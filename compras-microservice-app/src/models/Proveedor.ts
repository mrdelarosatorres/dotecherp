import { Entity, PrimaryGeneratedColumn, Column } from "typeorm";

@Entity({name:'cat_proveedores'})
export class Proveedor {
    @PrimaryGeneratedColumn({name:'id_proveedor'})
    idProveedor: number;
    @Column({name:'razon_social',length:200})
    razonSocial: string;
    @Column({name:'activo'})
    activo: boolean;

}