import { Entity, Column, PrimaryGeneratedColumn } from "typeorm";

@Entity({name:"cat_producto"})
export class Product {
    @PrimaryGeneratedColumn({name:'id_producto'})
    idProduct: number;
    @Column("text",{name:'codigo'})
    code: string;
}