import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { Transport, ClientsModule } from '@nestjs/microservices';
import { ProductoModule } from './modules/producto/producto.module';
import { GastoController } from './modules/gasto/gasto.controller';
import { GastoModule } from './modules/gasto/gasto.module';
import { OrdenDeCompraDeCostoModule } from './modules/orden-de-compra-de-costo/orden-de-compra-de-costo.module';
import { AppConfigModule } from './config/app/config/config.module';


@Module({
  imports: [
    ClientsModule.register([
      {
        name: 'COMPRAS_SERVICE', transport: Transport.TCP, options: {
          host: "127.0.0.1",
          port: 4001
        }
      },
    ]),
    ClientsModule.register([
      {
        name: 'CATALOGO_SERVICE', transport: Transport.TCP, options: {
          host: "127.0.0.1",
          port: 4002
        }
      },
    ]),
    ProductoModule,
    GastoModule,
    OrdenDeCompraDeCostoModule,
    AppConfigModule,
  ],
  controllers: [AppController, GastoController],
  providers: [AppService],
})
export class AppModule { }
