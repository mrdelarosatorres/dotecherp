import { Controller, Get, Inject } from '@nestjs/common';
import { AppService } from './app.service';
import { ClientProxy } from '@nestjs/microservices';
import { Message } from './messages/Message';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    @Inject('COMPRAS_SERVICE') private readonly client: ClientProxy,
    @Inject('CATALOGO_SERVICE') private readonly catalogoClient: ClientProxy) {

  }

  async onApplicationBootstrap() {
    await this.client.connect();
  }
  
  @Get()
  getHello(): string {
    let data = {
      id:1
    };
    const pattern = {cmd:'listProducts'};
    this.client.send<any>(pattern, new Message(JSON.stringify(data))).subscribe(data =>{
      console.log(data);
    });
    return this.appService.getHello();
  }
  @Get('testCatalogoMicroService')
  testCatalogoMicroService(): string {
    let data = {
      id:1
    };
    const pattern = {cmd:'listar'};
    this.catalogoClient.send<any>(pattern, new Message(JSON.stringify(data))).subscribe(data =>{
      console.log(data);
    });
    return 'testCatalogoMicroService';
  }
}
