import { Test, TestingModule } from '@nestjs/testing';
import { OrdenDeCompraDeCostoController } from './orden-de-compra-de-costo.controller';

describe('OrdenDeCompraDeCosto Controller', () => {
  let controller: OrdenDeCompraDeCostoController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [OrdenDeCompraDeCostoController],
    }).compile();

    controller = module.get<OrdenDeCompraDeCostoController>(OrdenDeCompraDeCostoController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
