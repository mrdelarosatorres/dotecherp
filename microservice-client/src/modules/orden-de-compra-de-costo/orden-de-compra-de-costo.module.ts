import { Module } from '@nestjs/common';
import { OrdenDeCompraDeCostoController } from './orden-de-compra-de-costo.controller';

@Module({
  controllers: [OrdenDeCompraDeCostoController]
})
export class OrdenDeCompraDeCostoModule {}
